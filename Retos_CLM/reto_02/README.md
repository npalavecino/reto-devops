#  Reto 2. Docker Compose

1.- Nginx que funcione como proxy reverso a nuesta app Nodejs
2.- Asegurar el endpoint /private con auth_basic
3.- Habilitar https y redireccionar todo el trafico 80 --> 443

# Instalar docker compose 

- En la documentación oficial hay más información de como instalar docker-compose: https://docs.docker.com/compose/install/

# Validar instalación 
```sh 
$ docker-compose --version 
```
#La configuración necesaria para construir las imagenes de docker esta en el archivo docker-compose.yml.

#Construir las imagenes
```sh
$ sudo docker-compose build
```
#Iniciar los Contenedores
```sh
$ sudo docker-compose up -d 
```
# Validar Aplicación 
# Para probar que la app esta ejecutandose ir a: localhost


# Los  requerimientos del reto se configuraron en archivo en  /nginx/nginx.conf


	#################################################
		Credenciales Aut_basic /private				       
		user:  admin	       			
		pass:  z84m^`sAp~954<_3         	
	#################################################
	

Se configura Nginx proxy reverso y se redirecciona trafico http --> https
Se genera certificado autofirmado 


