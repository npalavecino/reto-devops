// Instalar helm

```sh
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash
```
// Verificar si tenemos helm instalado

```sh
helm
```

// Configurar helm

```sh
helm init
```

// Verificar si Tiller está instalado

```sh
kubectl get pods -n kube-system
```

// Dar permisos a helm

```sh
kubectl create clusterrolebinding add-on-cluster-admin --clusterrole=cluster-admin --serviceaccount=kube-system:default
```


// Buscar paquetes

```sh
helm search
```

