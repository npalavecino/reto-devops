####  Reto 1. Dockerize la aplicación ####

1.-Construir la imagen más pequeña que pueda. Escribe un buen Docker

En este directorio se construira una imagen con una aplicación node.js 

## Instalar docker 
- MacOs: https://docs.docker.com/docker-for-mac/install/
- Ubuntu y otras distribuciones de linux: https://docs.docker.com/v17.12/install/linux/docker-ce/ubuntu/
- Windows: https://docs.docker.com/docker-for-windows/install/

## Crear una cuenta en docker hub 
Docker hub es un registry publico donde se encuentran varias imagenes de docker con diferentes tecnologias instaladas. La imagen que
se genero para este reto fue subido a docker-hub.
Es necesario crear una cuenta en  https://hub.docker.com/ para poder publicar la imagen resultante.


## Autenticarse en docker hub 
```sh 
#Ingresar usuario t contraseña cuando sea solicitada
$ docker login

```

## Construir imagen 
```sh 
$ docker build [DOCKERHUB_USER]/clm-app:v1
# docker build -t npalavecino/clm_app . 
```


## Correr la aplicacion
```sh
$ docker run -it -p $host_port:80 [DOCKERHUB_USER]/clm_app:v1
$ docker run -p 3000:3000 -d  npalavecino/clm_app

```
## Publicar la imagen
```sh
$ docker push [DOCKERHUB_USER]/clm_app:v1
```

