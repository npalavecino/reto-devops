FROM node:current-alpine3.10

# Create app directory
WORKDIR /usr/src/app

# Instalar dependencias app 
# A wildcard is used to ensure both package.json AND package-lock.json are copied

COPY package*.json ./

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "$(pwd)" \
    --no-create-home \
    "clm" \
    && npm install

USER clm

# Bundle app source
COPY . .

EXPOSE 3000

CMD [ "node", "index.js" ]
